//
//  SpanishWords.swift
//  SpanishVerbs
//
//  Created by Craig Niederberger on 12/31/15.
//  Copyright © 2015 Niedertronics. All rights reserved.
//

import Foundation

// The tenses, access strings with rawValue
enum Tense: String {
    case IndicativePresent = "Indicative Present"
    case IndicativePreterite = "Indicative Preterite"
    case IndicativeImperfect = "Indicative Imperfect"
    case IndicativeConditional = "Indicative Conditional"
    case IndicativeFuture = "Indicative Future"
    case SubjunctivePresent = "Subjunctive Present"
    case SubjunctiveImperfect = "Subjunctive Imperfect"
    case SubjunctiveImperfect2 = "Subjunctive Imperfect2"
    case SubjunctiveFuture = "Subjunctive Future"
    case Imperative = "Imperative"
    case PerfectPresent = "Perfect Present"
    case PerfectPreterite = "Perfect Preterite"
    case PerfectPast = "Perfect Past"
    case PerfectConditional = "Perfect Conditional"
    case PerfectFuture = "Perfect Future"
    case PerfectSubjunctivePresent = "Perfect Subjunctive Present"
    case PerfectSubjunctivePast = "Perfect Subjunctive Past"
    case PerfectSubjunctiveFuture = "Perfect Subjunctive Future"
    
    static let dictIndexString = [ 0: IndicativePresent, 1: IndicativePreterite, 2: IndicativeImperfect, 3: IndicativeConditional, 4: IndicativeFuture, 5: SubjunctivePresent, 6: SubjunctiveImperfect, 7: SubjunctiveImperfect2, 8: SubjunctiveFuture, 9: Imperative, 10: PerfectPresent, 11: PerfectPreterite, 12: PerfectPast, 13: PerfectConditional, 14: PerfectFuture, 15: PerfectSubjunctivePresent, 16: PerfectSubjunctivePast, 17: PerfectSubjunctiveFuture ]
    static let count = dictIndexString.count
    
    static let subjunctiveTenses = [ SubjunctivePresent, SubjunctiveImperfect, SubjunctiveImperfect2, SubjunctiveFuture, PerfectSubjunctivePresent, PerfectSubjunctivePast, PerfectSubjunctiveFuture ]
    
    static func indexValid( _ index: Int ) -> Bool {
        return 0 ... ( Tense.count - 1 ) ~= index ? true : false
    }
    
    static func fromIndex( _ index: Int ) -> Tense? {
        if indexValid( index ) {
            return Tense.dictIndexString[ index ]
        } else { return nil }
    }
    
    static func toIndex( _ theTense: Tense ) -> Int? {
        for ( index, value ) in dictIndexString {
            if theTense == value { return index }
        }
        return nil
    }
    
    static func stringFromIndex( _ index: Int ) -> String? {
        if indexValid( index ) {
            return Tense.dictIndexString[ index ]!.rawValue
        } else { return nil }
    }
    
    static func stringToIndex( _ theString: String ) -> Int? {
        for ( index, value ) in dictIndexString {
            if theString == value.rawValue { return index }
        }
        return nil
    }
    
    static func all() -> [ Tense ] {
        var allTenses = [ Tense ]()
        for ( _, value ) in dictIndexString { allTenses.append( value ) }
        return allTenses
    }
    
    static func isSubjunctive( _ theTense: Tense ) -> Bool {
        return subjunctiveTenses.contains( theTense ) ? true : false
    }
}

// The structure that contains all of the information to display and test a question
struct Question {
    var verbSpanish: String?        // canto
    var personalPronoun: String?    // I
    var verbEnglish: String?        // sing
    var note: String?               // optional note
}

// The main class that performs all the work of constructing and delivering the questions
class SpanishWords {
    
    var rawDataString: String // holds the raw data from loaded dictionary file
    var spanishDict = [[ String ]]() // rows are verbs, columns are tenses
    
    var intVerbTypes = [ Int ]() // column in the dictionary which represents the verb type for each verb
    var verbTypesCount = [ Int ]() // counts the number of each verb type loaded from the dictionary
    var pairs = [[ Int ]]() // verbs and tenses in columns to iterate through to get a random pair
    var pairsLeft = [[ Int ]]() // if until correct, container for pairs that still need to be tested
    var pairsCounter = 0 // keeps track of where you are in the queue of random pairs
    var pairsLeftCounter = 0 // keeps track of where you are in the container for pairs that still need to be tested
    var score = 0.0 // keeps track of the score
    
    let maxWords = 119 // the number of data in each line of the dictionary must always be 119:
    //  The format of the database held in spanish_dict.csv is as follows:
    //  Firstly, the Spanish infinitive [0]
    //  followed by the 5 English forms of present, present plural, past, present perfect, and gerund/participle
    //    e.g. eat, eats, ate, eaten, eating [1-5]
    //  followed by the Spanish verb type ( -ar regular, -er regular, etc. ) in file verb_types.txt, modifiable by the user [6]
    //  followed by an optional user entered note [7]
    //  followed by whether it can be reflexive (1) or not (0) [8]
    //  followed by the Spanish gerund then participle [9,10]
    //  followed by 6 Spanish forms of I, you informal, he/she/you formal, we, you all informal, they/you all formal for each of
    //    the 18 tenses:
    //    Indicative: Present [11-16], Preterite [17-22], Imperfect [23-28], Conditional [29-34], Future [35-40]
    //    Subjunctive: Present [41-46], Imperfect [47-52], Imperfect 2 [53-58], Future [59-64]
    //    Imperative [65-70] (Note that there is no Imperative yo form, and that - is recorded in that field for every verb.
    //      A design decision was made that that was worth the cost of the extra memory per verb.)
    //    Perfect: Present [71-76], Preterite [77-82], Past [83-88], Conditional [89-94], Future [95-100]
    //    Perfect Subjunctive: Present [101-106], Past [107-112], Future [113-118]
    //    That's 18 x 6 = 108 forms + the 2 gerund/participles + 1 type + +1 note + 1 reflexive + 5 english forms + 1 infinitive = 119 data each
    //  separated by commas, hence a csv file
    
    let spanishVerbOffset = 11 // hard coded offset for beginning of verb tenses in dictionary
    let verbTypeIndex = 6 // position of Spanish verb type
    var tenseRange: [ Tense : CountableClosedRange<Int> ] = [:] // maps Tense enums to ranges of tense indices in spanishDict
    let personalPronouns = [ "I", "You", "He", "We", "You all", "They" ]
    var errorString: String?
    var untilCorrectFlag = true // specifies whether or not go once through the stack or until all is correct
    var total = 0 // remembers how many pairs in the original shuffle
    var tries = 0 // counts the number of tries
    var maxVerbs = 50 // maximum number of verbs to test
    
    // MARK: - Initializers
    
    init() {
        
        do {
            rawDataString = try String( // load in the raw data from the dictionary file
                contentsOfFile: "/Users/craign/code/ruby/spanish/spanish_dict.csv",
                encoding: String.Encoding.utf8 )
        } catch let error as NSError {
            errorString = error.description
            rawDataString = ""
            return
        }
        
        for line in rawDataString.components( separatedBy: "\n" ) { // load into the dictionary 2D arrary
            
            if line.trimmingCharacters( in: CharacterSet.whitespaces ) != "" { // ignore whitespace only
        
                let words = line.components( separatedBy: "," ) // rows are verbs, columns are tenses
                
                if words.count == maxWords {
                    spanishDict.append( words )
                } else {
                #if DEBUG
                    print( "WARNING: \( words[ 0 ] ) not loaded, number verbs \( words.count ) not \( maxWords )" )
                #endif
                }
            }
        }
        
        guard spanishDict.count > 0 else { fatalError( "Number of verbs loaded was zero." ) }

        // Now that the dictionary is loaded, count the verb types
        let verbTypes = spanishDict[ column: verbTypeIndex ]
        
        // All in spanishDict is a string, verb type is int, needs to be changed into an array of int
        for verbType in verbTypes {
            guard let intVerbType = Int( verbType ) else {
                fatalError( "Something went terribly wrong in verb type int conversion" )
            }
            
            intVerbTypes.append( intVerbType )
        }
        
        guard intVerbTypes.max() != nil else { fatalError( "Something went terribly wrong in verb types counting" ) }
        
        verbTypesCount = [ Int ]( repeating: 0, count: intVerbTypes.max()! + 1 )
        for value in intVerbTypes { verbTypesCount[ value ] += 1 }
        
        // Created by Ruby code tense-coder.rb operating on list of tenses in file tenses.txt
        //   I tried to do this with one big let tenseRange = [ ..., but the compiler choked
        tenseRange[ Tense.IndicativePresent ] = ( 11 ... 16 )
        tenseRange[ Tense.IndicativePreterite ] = ( 17 ... 22 )
        tenseRange[ Tense.IndicativeImperfect ] = ( 23 ... 28 )
        tenseRange[ Tense.IndicativeConditional ] = ( 29 ... 34 )
        tenseRange[ Tense.IndicativeFuture ] = ( 35 ... 40 )
        tenseRange[ Tense.SubjunctivePresent ] = ( 41 ... 46 )
        tenseRange[ Tense.SubjunctiveImperfect ] = ( 47 ... 52 )
        tenseRange[ Tense.SubjunctiveImperfect2 ] = ( 53 ... 58 )
        tenseRange[ Tense.SubjunctiveFuture ] = ( 59 ... 64 )
        tenseRange[ Tense.Imperative ] = ( 65 ... 70 )
        tenseRange[ Tense.PerfectPresent ] = ( 71 ... 76 )
        tenseRange[ Tense.PerfectPreterite ] = ( 77 ... 82 )
        tenseRange[ Tense.PerfectPast ] = ( 83 ... 88 )
        tenseRange[ Tense.PerfectConditional ] = ( 89 ... 94 )
        tenseRange[ Tense.PerfectFuture ] = ( 95 ... 100 )
        tenseRange[ Tense.PerfectSubjunctivePresent ] = ( 101 ... 106 )
        tenseRange[ Tense.PerfectSubjunctivePast ] = ( 107 ... 112 )
        tenseRange[ Tense.PerfectSubjunctiveFuture ] = ( 113 ... 118 )
    }
    
    // Shuffles the deck! creates the 2D array structure to iterate through to get random words
    func shuffle( _ tenseArray: [ Tense ], _ number: Int, _ verbTypeArray: [ Int ], _ untilCorrect: Bool ) {
        // Takes an array of Tenses as argument, built from the checkboxes in the controller
        // and the number of verbs (1 to 6) to pull from each tense
        // and an array indicating which verb types to use (1-indexed)
        // and whether it will test until all answers are correct (true/false)
        
        untilCorrectFlag = untilCorrect
        
        // Go through the tenses checked off and passed to this function to build array
        var indices = [ Int ]()
        for tense in tenseArray {
            if let aRange = tenseRange[ tense ] { // unwrap the Dictionary return optional
                indices.append( contentsOf: aRange )
            }
        }
        
        var deck = [[ Int ]]() // 2D deck of indices to shuffle
        var verbIndices = [ Int ]() // for shuffling the verb order each time
        for ( index, intVerbType ) in intVerbTypes.enumerated() {
            if verbTypeArray.contains( intVerbType ) { // add it only if the verb type is selected
                deck.append( shuffleSubset( indices, number ) )
                verbIndices.append( index )
            }
        }
        
        pairs.removeAll()
        indices = indexArray( verbIndices.count )
        for tense in 0 ..< number * tenseArray.count {
            for index in indices.shuffled() {
                let verb = verbIndices[ index ]
                pairs.append( [ verb, deck[ index ][ tense ] ] ) // then add to the container holding the row and column pairs
            }
        }
        
        pairs = pairs.filter(){ $0[ 1 ] != 65 } // first person imperative doesn't exist, so remove those pairs
        
        // If the number of pairs is greater than the maximum allowed, truncate the container
        if pairs.count > maxVerbs {
            pairs.removeLast( pairs.count - maxVerbs )
        }
        
        pairsCounter = 0 // reset the iterator counter
        score = 0.0 // reset the score
        total = pairs.count
        
        if untilCorrectFlag {
            pairsLeft = pairs
            pairsLeftCounter = 0
        }
    }
    
    // Takes an array of indices and returns an equal number from 1 to 6 of each of 6 sequential items, randomized,
    //   so that a subset of tenses can be quizzed
    func shuffleSubset( _ arr: [ Int ], _ count: Int ) -> [ Int ] {
        var indexSubset: [ Int ] = []
        let size = arr.count / 6
        
        for counter in 0 ..< size {
            // indexSubset.append( arr[ counter * 6 ..< ( counter + 1 ) * 6 ].shuffled()[ 0 ..< count ] )
            indexSubset += arr[ counter * 6 ..< ( counter + 1 ) * 6 ].shuffled()[ 0 ..< count ]
        }
        return indexSubset.shuffled()
    }
    
    // MARK: - Accessors
    
    func error() -> String? {
        return errorString
    }
    
    func get( _ row: Int, _ col: Int ) -> String? {
        guard row >= 0 && row < spanishDict.count && col >= 0 && col < maxWords else { return nil }
        return spanishDict[ row ][ col ]
    }
    
    func printPairsDict( _ pairsIn: [[ Int ]] ) {
        for ( index, pair ) in pairsIn.enumerated() {
            print( NSString( format: "%3d, %3d, %3d,", index, pair[ 0 ], pair[ 1 ] ), spanishDict[ pair[ 0 ] ][ pair[ 1 ] ] )
        }
    }
    
    func index2tense( _ index: Int ) -> Tense? {
        for ( tense, range ) in tenseRange {
            if range.contains( index ) {
                return tense
            }
        }
        return nil
    }
    
    func getMaxVerbs() -> Int {
        return maxVerbs
    }
    
    func setMaxVerbs( _ number: Int ) {
        guard number >= 0 else {
            #if DEBUG
                print( "ERROR: setMaxVerbs called with <= 0" )
                return
            #else
                fatalError( "Something went terribly wrong in setMaxVerbs" )
            #endif
        }
        maxVerbs = number
    }
    
    func getCount() -> Int {
        return pairs.count
    }
    
    // MARK: - Pairs accessors and utilities
    
    func getPercentComplete() -> Double {
        return Double( pairsCounter + 1 ) / Double( total ) * 100.0
    }
    
    func getDone() -> Bool { // returns if we're finished with the test
        return untilCorrectFlag ? pairsLeft.count == 0 : total == pairsCounter + 1
    }
    
    func getVerbSpanish() -> String? { // gets verb from dictionary by stem and tense index in pairs, returns nil if can't get stem
        
        guard pairsCounter < pairs.count else { return nil } // prevent from extending beyond pairs container
        
        return ( spanishDict[ pairs[ pairsCounter ][ 0 ] ][ pairs[ pairsCounter ][ 1 ] ] )
    }
    
    func getTenseNumber( _ index: Int ) -> Int { // returns 0 through 5 to specify which one of six tenses a tense index is
        return ( index - spanishVerbOffset ) % 6
    }
    
    // MARK: - Quesiton accessors and utilities
    
    // MARK: Place 1 of 2 that needs to be updated when adding tenses
    func serEnglish( _ tenseNumber: Int, tense: Tense ) -> String {
        
        switch tense {
        case .IndicativePresent, .Imperative, .SubjunctivePresent:
            switch tenseNumber {
            case 0:
                return "am"
            case 2:
                return "is"
            default:
                return "are"
            }
            
        case .IndicativePreterite, .SubjunctiveImperfect:
            if [ 0, 2 ].contains( tenseNumber ) { return "was" } else { return "were" }
            
        case .IndicativeImperfect:
            return "used to be"
            
        case .IndicativeConditional:
            return "would be"
            
        case .IndicativeFuture:
            return "will be"
            
        case .PerfectPresent, .PerfectSubjunctivePresent:
            if tenseNumber == 2 { return "has been" } else { return "have been" }
            
        case .PerfectPast, .PerfectSubjunctivePast:
            return "had been"
            
        case .PerfectFuture:
            return "will have been"
            
        case .PerfectConditional:
            return "would have been"
        
        default:
        #if DEBUG
            print( "WARNING: serEnglish not yet coded for \( tense )" )
            return ""
        #else
            fatalError( "Something went terribly wrong in constructing the English phrase" )
        #endif
            
        }
    }
    
    func getQuestion() -> Question {
        
        var myQuestion = Question()
        
        if let verb = getVerbSpanish() {
            
            myQuestion.verbSpanish = verb
            let verbIndex = pairs[ pairsCounter ][ 0 ] // index to verb row in spanishDict
            let verbStem = spanishDict[ verbIndex ][ 0 ]
            let tenseIndex = pairs[ pairsCounter ][ 1 ] // index to tense column in spanishDict
            
            let verbNote = spanishDict[ verbIndex ][ 7 ] // the note
            if verbNote.trimmingCharacters( in: CharacterSet.whitespaces ) != "" {
                myQuestion.note = verbNote
            }
            
            let tenseNumber = getTenseNumber( tenseIndex ) // 0-5 for the 6 forms of each tense
            myQuestion.personalPronoun = personalPronouns[ tenseNumber ]
    
            guard index2tense( tenseIndex ) != nil else { fatalError( "An impossible tense occurred in index2tense" ) }
            let tense = index2tense( tenseIndex )!
            
            if Tense.isSubjunctive( tense ) {
                let pronoun = myQuestion.personalPronoun! == "I" ? myQuestion.personalPronoun! : myQuestion.personalPronoun!.lowercased()
                myQuestion.personalPronoun = "(I hope that) " + pronoun
            }

            if [ "ser", "estar" ].contains( verbStem ) { // the insanely irregular to be
                myQuestion.verbEnglish = serEnglish( tenseNumber, tense: tense )
            } else {
                
            // MARK: Place 2 of 2 that needs to be updated when adding tenses
                
                // Indicative Present, Imperative, SubjunctivePresent
                if [ Tense.IndicativePresent, Tense.Imperative, Tense.SubjunctivePresent ].contains( index2tense( tenseIndex )! ) {
                    if tenseNumber == 2 {
                        myQuestion.verbEnglish = spanishDict[ verbIndex ][ 2 ]
                    } else {
                        myQuestion.verbEnglish = spanishDict[ verbIndex ][ 1 ]
                    }
                    // For the tenses that need it, question mark tag test and replace if found
                    myQuestion.verbEnglish!.replaceQuestionMark( serEnglish( tenseNumber, tense: tense ) )
                }
                
                if index2tense( tenseIndex ) == .Imperative {
                    myQuestion.verbEnglish = myQuestion.verbEnglish! + "!"
                }
                
                // Indicative Preterite, Imperfect Subjunctive
                if [ Tense.IndicativePreterite, Tense.SubjunctiveImperfect ].contains( index2tense( tenseIndex )! ) {
                    myQuestion.verbEnglish = spanishDict[ verbIndex ][ 3 ]
                    myQuestion.verbEnglish!.replaceQuestionMark( serEnglish( tenseNumber, tense: tense ) )
                }
                
                // Indicative Imperfect
                if index2tense( tenseIndex ) == .IndicativeImperfect {
                    myQuestion.verbEnglish = "used to \( spanishDict[ verbIndex ][ 1 ] )"
                    myQuestion.verbEnglish!.replaceQuestionMark( serEnglish( tenseNumber, tense: tense ) )
                }
                
                // Indicative Conditional
                if index2tense( tenseIndex ) == .IndicativeConditional {
                    myQuestion.verbEnglish = "would \( spanishDict[ verbIndex ][ 1 ] )"
                    myQuestion.verbEnglish!.replaceQuestionMark( serEnglish( tenseNumber, tense: tense ) )
                }
                
                // Indicative Future
                if index2tense( tenseIndex ) == .IndicativeFuture {
                    myQuestion.verbEnglish = "will \( spanishDict[ verbIndex ][ 1 ] )"
                    myQuestion.verbEnglish!.replaceQuestionMark( serEnglish( tenseNumber, tense: tense ) )
                }
                
                // Perfect Present, Perfect Subjunctive Present
                if [ Tense.PerfectPresent, Tense.PerfectSubjunctivePresent ].contains( index2tense( tenseIndex )! ) {
                    var preceding = "have"
                    if tenseNumber == 2 { preceding = "has" }
                    myQuestion.verbEnglish = preceding + " " + spanishDict[ verbIndex ][ 4 ]
                    myQuestion.verbEnglish!.replaceQuestionMark( "been" )
                }
                
                // Perfect Past, Perfect Subjunctive Past
                if [ Tense.PerfectPast, Tense.PerfectSubjunctivePast ].contains( index2tense( tenseIndex )! ) {
                    myQuestion.verbEnglish = "had \( spanishDict[ verbIndex ][ 4 ] )"
                    myQuestion.verbEnglish!.replaceQuestionMark( "been" )
                }
                
                // Perfect Future
                if index2tense( tenseIndex ) == .PerfectFuture {
                    myQuestion.verbEnglish = "will have \( spanishDict[ verbIndex ][ 4 ] )"
                    myQuestion.verbEnglish!.replaceQuestionMark( "been" )
                }
                
                // Perfect Conditional
                if index2tense( tenseIndex ) == .PerfectConditional {
                    myQuestion.verbEnglish = "would have \( spanishDict[ verbIndex ][ 4 ] )"
                    myQuestion.verbEnglish!.replaceQuestionMark( "been" )
                }
                
            }
        }
        
        return myQuestion
    }
    
    func nextQuestion() -> Bool { // advances and returns false if another question can't be asked... e.g. at end
        
        if pairsCounter < ( pairs.count - 1 ) {
            pairsCounter += 1
            if untilCorrectFlag { pairsLeftCounter += 1 }
            return true
        }
        else {
            if !untilCorrectFlag { return false }
            else {
                if pairsLeft.count == 0 { return false }
                else {
                    pairs = pairsLeft.shuffled()
                    pairsLeft = pairs
                    pairsCounter = 0
                    pairsLeftCounter = 0
                    return true
                }
            }
        }
    }
    
    // MARK: - Handlers
    
    func compareGuess( _ guess: String ) -> String {
        
        tries += 1
        
        // Trim whitespace from guess front and back, and convert to lowercase, which is the case of the dictionary
        let trimmedString = guess.trimmingCharacters( in: .whitespacesAndNewlines ).stringByCollapsingInnerWhitespace().lowercased()
        
        if let verb = getVerbSpanish() {
            
            // For answers that have colons (:) e.g. "hubieras descrito:descripto", need to support both full forms,
            //   e.g. "hubieras descrito" and "hubieras descripto"
            var answerArray = [ String ]()
            answerArray = verb.characters.split{ $0 == ":" }.map( String.init )
            
            if answerArray.count > 1 { // there's a colon
                let subArray = answerArray[ 0 ].characters.split{ $0 == " " }.map( String.init )
                if subArray.count > 1 { // there was a preceding word that needs to be added to both
                    answerArray[ 1 ] = subArray[ 0 ] + " " + answerArray[ 1 ]
                }
            }
            
            if answerArray.contains( trimmedString ) {
                score += 1.0
                
                if untilCorrectFlag {
                    pairsLeft.remove( at: pairsLeftCounter )
                    pairsLeftCounter -= 1
                }
                
                return "Correcto"
            }
            else if correctExceptAccents( answerArray[ 0 ], trimmedString ) {
                score += 0.5
                return "Correcto excepto acentos: \( answerArray[ 0 ] )"
            }
            else if answerArray.count == 2 && correctExceptAccents( answerArray[ 1 ], trimmedString ) {
                score += 0.5
                return "Correcto excepto acentos: \( answerArray[ 1 ] )"
            }
            else if answerArray.count == 1 {
                return "Incorrecto: \( answerArray[ 0 ] )"
            }
            else if answerArray.count == 2 {
                return "Incorrecto: \( answerArray[ 0 ] ) or \( answerArray[ 1 ] )"
            }
        }
        
        return "Error in compareGuess!"
    }
    
    func correctExceptAccents( _ aString: String, _ bString: String ) -> Bool {
        return aString.folding( options: .diacriticInsensitive, locale: Locale.current ) == bString.folding( options: .diacriticInsensitive, locale: Locale.current )
    }
    
    func getProgress() -> Double {
        return score / Double( total ) * 100.0
    }
    
    func getShortScore() -> String {
        return untilCorrectFlag ? String( format: "%d tries for %d verbs", tries, total ) : String( format: "%.1f %%",  getProgress() )
    }
    
    func getLongScore() -> String {
        return untilCorrectFlag ? "It took " + getShortScore() : "You got " + getShortScore()
    }
    
}
