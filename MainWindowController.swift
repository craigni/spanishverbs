//
//  MainWindowController.swift
//  SpanishVerbs
//
//  Created by Craig Niederberger on 12/31/15.
//  Copyright © 2015 Niedertronics. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController {
    
    @IBOutlet weak var questionText: NSTextField!
    @IBOutlet weak var guessText: NSTextField!
    @IBOutlet weak var noteText: NSTextField!
    @IBOutlet weak var answerText: NSTextField!
    @IBOutlet weak var scoreText: NSTextField!
    @IBOutlet weak var startButton: NSButton!
    @IBOutlet weak var guessButton: NSButton!
    @IBOutlet weak var percentComplete: NSProgressIndicator!
    @IBOutlet weak var scoreProgress: NSProgressIndicator!
    @IBOutlet weak var verbsPerTense: NSPopUpButton!
    @IBOutlet weak var verbsPerTenseLabel: NSTextField!
    @IBOutlet weak var tenseTableLabel: NSTextField!
    @IBOutlet weak var tenseTable: NSTableView!
    @IBOutlet weak var verbTypesTableLabel: NSTextField!
    @IBOutlet weak var verbTypeTable: NSTableView!
    @IBOutlet weak var untilCorrectButton: NSButton!
    @IBOutlet weak var maxVerbsLabel: NSTextField!
    @IBOutlet weak var maxVerbs: NSTextField!
    
    let mySpanishWords = SpanishWords()
    var myQuestion = Question()
    
    var rawDataString: String = "" // holds the raw data from loaded files
    
    let synth = NSSpeechSynthesizer()
    
    // MARK: Needs to be updated when adding tenses
    let tenseNames = [ Tense.IndicativePresent.rawValue, Tense.IndicativePreterite.rawValue, Tense.IndicativeImperfect.rawValue, Tense.IndicativeConditional.rawValue, Tense.IndicativeFuture.rawValue, Tense.PerfectPresent.rawValue, Tense.PerfectPast.rawValue, Tense.Imperative.rawValue, Tense.SubjunctivePresent.rawValue, Tense.SubjunctiveImperfect.rawValue, Tense.PerfectFuture.rawValue, Tense.PerfectConditional.rawValue, Tense.PerfectSubjunctivePresent.rawValue, Tense.PerfectSubjunctivePast.rawValue ]
    
    var tenseArray = [ Tense ]() // initial choice for tenses
    var verbTypeNames = [ String ]()
    var verbTypeArray = [ Int ]() // initial choice for verb types
    
    init() {
        super.init( window: nil )
        
        if mySpanishWords.error() != nil { fatalError( "Error loading dictionary file" ) }
        
        do {
            rawDataString = try String( // load in the raw data from the dictionary file
                contentsOfFile: "/Users/craign/code/ruby/spanish/verb_types.txt",
                encoding: String.Encoding.utf8 )
        } catch let error as NSError { print( "Error loading verb types file: \( error )" ) }
        
        // Load the verb types hash
        var verbTypes: [ Int : String ] = [:]
        for line in rawDataString.components( separatedBy: "\n" ) {
            
            if line.trimmingCharacters( in: CharacterSet.whitespaces ) != "" { // ignore whitespace only
                
                let type = line.components( separatedBy: "," ) // rows are verbs, columns are tenses
                
                guard type.count == 2 else {
                    fatalError( "Error processing verb types file: line length not 2" )
                }
                
                guard let intType = Int( type[ 0 ] ) else {
                    fatalError( "Error processing verb types file: type not int" )
                }
                
                verbTypes[ intType ] = type[ 1 ]
            }
        }
        
        for verbTypeKey in 1 ... verbTypes.count {
            verbTypeNames.append( verbTypes[ verbTypeKey ]! )
        }

        // load the contents of the nib, and set the owner as self, which connects the oultlets
        Bundle.main.loadNibNamed( windowNibName, owner: self, topLevelObjects: nil )
        
        // Initial selection of Verb Types Table
        let selectedIndicesVerbTypesTable = NSMutableIndexSet()
        verbTypeArray = [ 1 ] // set initial choice data structure, note 1 indexed, which is different than table
        selectedIndicesVerbTypesTable.add( 0 ) // set initial choice in table
        verbTypeTable.selectRowIndexes( selectedIndicesVerbTypesTable as IndexSet, byExtendingSelection: false )
        
        // Initial selection of Tense Table
        tenseArray = [ Tense.IndicativePresent ] // set initial choice data structure
        let selectedIndicesTenseTable = NSMutableIndexSet()
        selectedIndicesTenseTable.add( 0 ) // set initial choice in table
        tenseTable.selectRowIndexes( selectedIndicesTenseTable as IndexSet, byExtendingSelection: false )
        
        synth.setVoice( "com.apple.speech.synthesis.voice.paulina" ) // female Spanish voice

        startUI()
    }
    
    required init?( coder: NSCoder ) {
        super.init( coder: coder )
    }
    
    override var windowNibName: String {
        return "MainWindowController"
    }
    
    func getTableSelections() { // creates the arrays to pass to the shuffle routine
        
        // First the tense array
        tenseArray.removeAll()
        for index in getIndices( tenseTable.selectedRowIndexes ) {
            tenseArray.append( Tense( rawValue: tenseNames[ index ] )! ) // know that this must be a valid rawValue, because it was built from them
        }
        
        // Then the verb type array, remember it is 1-indexed
        verbTypeArray.removeAll()
        verbTypeArray = getIndices( verbTypeTable.selectedRowIndexes ).map{ $0 + 1 }
    
        showCount()
    }
    
    @IBAction func updateVerbsPerTense( _ sender: NSPopUpButton ) {
        showCount()
    }
    
    func clearTextFields() {
        noteText.stringValue = ""
        answerText.stringValue = ""
        scoreText.stringValue = ""
    }
    
    func resetScore() {
        percentComplete.doubleValue = 0.0
        scoreProgress.doubleValue = 0.0
    }
    
    func showCount() { // shows how many questions there are, updated with each UI interaction
        mySpanishWords.shuffle( tenseArray, verbsPerTense.indexOfSelectedItem + 1, verbTypeArray, untilCorrectButton.state == 1 ? true : false )
        scoreText.stringValue = "\( mySpanishWords.getCount() ) verbs"
    }
    
    @IBAction func getMaxVerbsText( _ sender: NSTextFieldCell ) {
        
        guard maxVerbs.stringValue.trimmingCharacters( in: CharacterSet.whitespaces ) != "" else {
            maxVerbs.stringValue = "\( mySpanishWords.getMaxVerbs() )"
            popup( "Enter Maximum Number", informativeText: "Maximum number verbs can't be blank", alertStyle: .warning )
            return
        }
        
        guard let maxVerbsNumber = isInteger( incoming: maxVerbs.stringValue ) else {
            maxVerbs.stringValue = "\( mySpanishWords.getMaxVerbs() )"
            popup( "Enter Maximum Number", informativeText: "Maximum number verbs must be an integer", alertStyle: .warning )
            return
        }
        
        guard maxVerbsNumber >= 10 && maxVerbsNumber <= 1000 else {
            maxVerbs.stringValue = "\( mySpanishWords.getMaxVerbs() )"
            popup( "Enter Maximum Number", informativeText: "Maximum number verbs must be between 10 and 1000", alertStyle: .warning )
            return
        }
        
        mySpanishWords.setMaxVerbs( maxVerbsNumber )
        showCount()
    }
    
    // MARK: Every time you add a widget, consider enabling it here
    func enableWidgets( _ state: Bool ) {
        startButton.isEnabled = state
        guessButton.isEnabled = !state
        guessText.isEnabled = !state
        verbsPerTense.isEnabled = state
        verbsPerTenseLabel.isEnabled = state
        tenseTableLabel.isEnabled = state
        tenseTable.isEnabled = state
        verbTypeTable.isEnabled = state
        untilCorrectButton.isEnabled = state
        maxVerbs.isEnabled = state
        
        let currentTextColor = state ? NSColor.textColor : NSColor.textBackgroundColor
        verbsPerTenseLabel.textColor = currentTextColor
        tenseTableLabel.textColor = currentTextColor
        verbTypesTableLabel.textColor = currentTextColor
        maxVerbsLabel.textColor = currentTextColor
        
        let currentTableColor = state ? NSColor.textBackgroundColor : NSColor.windowBackgroundColor
        tenseTable.backgroundColor = currentTableColor
        verbTypeTable.backgroundColor = currentTableColor
        
        guessText.backgroundColor = state ? NSColor.windowBackgroundColor : NSColor.textBackgroundColor
    }
    
    func startUI() {        
        questionText.stringValue = "Press Start to begin"
        verbsPerTense.selectItem( at: 5 ) // start with all verbs
        enableWidgets( true )
        percentComplete.isHidden = false
        clearTextFields()
        resetScore()
        mySpanishWords.setMaxVerbs( 50 )
        maxVerbs.stringValue = "\( mySpanishWords.getMaxVerbs() )"
        showCount()
    }
    
    @IBAction func startQuiz( _ sender: NSButton ) {
        
        guard tenseArray.count > 0 else {
            popup( "Select Tense(s)", informativeText: "You need to choose at least one tense", alertStyle: .warning )
            return
        }
        
        guard verbTypeArray.count > 0 else {
            popup( "Select Verb Type(s)", informativeText: "You need to choose at least one verb type", alertStyle: .warning )
            return
        }
                
        enableWidgets( false )
        
        if untilCorrectButton.state == 1 {
            percentComplete.isHidden = true
        }
        
        resetScore()
        showQuestion()
    }
    
    func showQuestion() {
        clearTextFields()
        
        myQuestion = mySpanishWords.getQuestion()
        guard myQuestion.personalPronoun != nil && myQuestion.verbEnglish != nil else {
            fatalError( "Something went terribly wrong in trying to get a question" )
        }
        
        questionText.stringValue = "\( myQuestion.personalPronoun! ) \( myQuestion.verbEnglish! )"
        if myQuestion.note != nil { noteText.stringValue = myQuestion.note! }
        self.guessText.becomeFirstResponder()
    }
    
    @IBAction func guessQuiz( _ sender: NSButton ) {
        
        switch guessButton.title {
            
        case "Answer" :
            // First check for whitespace only in guess
            if guessText.stringValue.trimmingCharacters( in: CharacterSet.whitespaces ) == ""
            {
                popup( "Enter Guess", informativeText: "Please enter a guess", alertStyle: .warning )
            }
            else
            {
                answerText.stringValue = mySpanishWords.compareGuess( guessText.stringValue )
                _ = answerText.stringValue == "Correcto" ? synth.startSpeaking( guessText.stringValue ) : synth.startSpeaking( answerText.stringValue )
                guessText.isEnabled = false
                percentComplete.doubleValue = mySpanishWords.getPercentComplete()
                scoreProgress.doubleValue = mySpanishWords.getProgress()
                guessButton.title = "Continue"
                
                if mySpanishWords.getDone() {
                    scoreText.stringValue = mySpanishWords.getShortScore()
                    popup( "Score", informativeText: mySpanishWords.getLongScore(), alertStyle: .informational )
                }
            }
            
        case "Continue" :
            guessText.stringValue = ""
            guessButton.title = "Answer"
            guessText.isEnabled = true
            
            if mySpanishWords.nextQuestion() {
                showQuestion()
            } else {
                startUI()
            }
            
        default :
            fatalError( "Strange guess button!" )
        }
    }
}

// MARK: - NSTableViewDataSource
extension MainWindowController: NSTableViewDataSource {
    func numberOfRows( in tableView: NSTableView ) -> Int {
        if tableView == tenseTable { return tenseNames.count }
        else { return verbTypeNames.count }
    }
    
    func tableView( _ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let cellView: NSTableCellView = tableView.make(withIdentifier: tableColumn!.identifier, owner: self) as! NSTableCellView
        cellView.textField!.stringValue = tableView == tenseTable ? tenseNames[ row ] : verbTypeNames[ row ]
        return cellView
    }
}

func getIndices( _ indexSet: IndexSet ) -> [ Int ] {
    var mySelectedRows = [ Int ]()
    ( indexSet as NSIndexSet).enumerate(using: { ( index, stop ) -> Void in
        mySelectedRows.append( index )
    } )
    return mySelectedRows
}

// MARK: - NSTableViewDelegate
extension MainWindowController: NSTableViewDelegate {
    func tableViewSelectionDidChange( _ notification: Notification ) {
        getTableSelections()
    }
}
