//
//  MyPopups.swift
//  SpanishVerbs
//
//  Created by Craig Niederberger on 1/17/16.
//  Copyright © 2016 Niedertronics. All rights reserved.
//

import Cocoa

// alertStyle choices are .WarningAlertStyle, .InformationalAlertStyle, and .CriticalAlertStyle

func popup( _ messageText: String, informativeText: String, alertStyle: NSAlertStyle ) {
    let alert = NSAlert()
    alert.messageText = messageText
    alert.informativeText = informativeText
    alert.alertStyle = alertStyle
    alert.addButton( withTitle: "OK" )
    alert.runModal()
}

func erroroutPopup( _ messageText: String, informativeText: String ) {
    popup( messageText, informativeText: informativeText, alertStyle: .critical )
    // NSApp.performSelector( "terminate:", withObject: nil, afterDelay: 0.0 ) // exit app
    NSApp.terminate( nil )
}
