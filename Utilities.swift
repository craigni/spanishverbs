//
//  Utilities.swift
//  SpanishVerbs
//
//  Created by Craig Niederberger on 1/9/16.
//  Copyright © 2016 Niedertronics. All rights reserved.
//

import Cocoa
import Foundation

// Fisher-Yates shuffle algorithm, thanks http://stackoverflow.com/questions/24026510/how-do-i-shuffle-an-array-in-swift

extension Collection { // return a copy of self with elements shuffled
    func shuffled() -> [ Generator.Element ] {
        var list = Array( self )
        list.shuffle()
        return list
    }
}

// Swift 3 update, thanks http://stackoverflow.com/questions/37843647/shuffle-array-swift-3

extension MutableCollection where Index == Int { // shuffle elements of self in place
    
    mutating func shuffle() {
        
        if count < 2 { return } // empty and single-element collections don't shuffle
        
        for i in startIndex ..< endIndex - 1 {
            let j = Int( arc4random_uniform( UInt32( endIndex - i ) ) ) + i
            guard i != j else { continue }
            swap( &self[ i ], &self[ j ] )
        }
    }
}

// Get a column from a 2D array, use like A[ column: 3 ]

extension Array where Element : Collection {
    subscript( column column : Element.Index ) -> [ Element.Iterator.Element ] {
        return map { $0[ column ] }
    }
}

// Create and return an index array

func indexArray( _ count: Int ) -> [ Int ] {
    var array = [ Int ]()
    array.append( contentsOf: 0 ..< count )
    return array
}

// For quickly printing 2D arrays to console for debugging

func print2Darray <T> ( _ arr: [[ T ]] ) {
    
    for r in arr {
        for c in r {
            print( c, terminator:" " )
        }
        print( "" )
    }
}

// Replaces multiple occurences of whitespace within a string with a single space

extension String {
    func stringByCollapsingInnerWhitespace() -> String {
        var returnString = ""
        if let regex = try? NSRegularExpression( pattern: "\\s+", options: [] ) {
            returnString = regex.stringByReplacingMatches( in: self, options: [], range: NSMakeRange( 0, self.characters.count ), withTemplate: " " )
        }
        return returnString
    }
}

// Replaces a question mark tag with a string

extension String {
    mutating func replaceQuestionMark( _ replaceString: String ) {
        if  self.range( of: "?" ) != nil {
            self = self.replacingOccurrences( of: "?" , with: replaceString )
        }
    }
}

// Returns an integer if a string is an integer, nil if not, accepts leading and trailing whitespace

func isInteger( incoming: String ) -> Int? {
    
    let formatter: NumberFormatter = {
        let _formatter = NumberFormatter()
        _formatter.numberStyle = .decimal
        return _formatter
    }()
    
    let locale: Locale = Locale.current
    let thousandSeparator: String = (locale as NSLocale).object( forKey: NSLocale.Key.groupingSeparator ) as! String
    
    guard let value = formatter.number( from: incoming ) else {
        return nil
    }
    
    if value as Int != Int( incoming.replacingOccurrences( of: thousandSeparator, with: "" ).trimmingCharacters( in: .whitespacesAndNewlines ) ) {
        return nil
    }
    
    return value.intValue
}
